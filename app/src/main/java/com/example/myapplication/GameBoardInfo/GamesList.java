package com.example.myapplication.GameBoardInfo;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.myapplication.R;
import com.example.myapplication.databinding.ActivityGameBoardInformationBinding;
//import com.example.myapplication.databinding.FragmentSecond2Binding;
import com.example.myapplication.databinding.FragmentGamesListBinding;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.Source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GamesList extends Fragment {

    private FirebaseFirestore databaseLoginInfoConnection;
    private FragmentGamesListBinding binding;

    String games[]
            = { "Checkers", "Chess", "Risk", "Sorry!",
            "Monopoly", "The Game of Life", "Connect Four",
            "Scrabble", "Catan", "Candy Land", "Pandemic",
            "Mouse Trap", "Clue", "Battleship", "Stratego",
            "Backgammon", "Blokus", "Operation", "Don't Break the Ice",
            "Hungry Hungry Hippos", "Boggle", "Jenga", "Mastermind",
            "Uno", "Dominoes", "Chutes and Ladders", "Yahtzee",
            "Apples to Apples", "Pictionary"


    };



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentGamesListBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        databaseLoginInfoConnection = FirebaseFirestore.getInstance();
        CollectionReference colRef = databaseLoginInfoConnection.collection("BoardGames");
        CollectionReference rankedRef = databaseLoginInfoConnection.collection("data2018");


/*
        CollectionReference ranked = databaseLoginInfoConnection.collection("data2018")
                .orderBy("rank", Query.Direction.ASCENDING).limit(10)
                .get();
*/



        ArrayList<String> fullList = new ArrayList<>();
        ArrayAdapter<String> tasks = new ArrayAdapter<>(getActivity(),
                R.layout.gameslist_item_view, R.id.itemTextView,
                fullList);

        ListView listTasks = getActivity().findViewById(R.id.listView);
        listTasks.setAdapter(tasks);


        rankedRef.get()
                .addOnCompleteListener(task -> {;
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, document.getId() + " => " + document.getData());

                            fullList.add(document.getString("names"));;
                            //adds all doc IDs in SavedIdeas collection
                            tasks.notifyDataSetChanged();
                            //notifies adapter of change in ListView

                            if (document.exists()) {

                            } else {
                                Log.d(TAG, "No such document");
                            }
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });



        listTasks.setOnItemClickListener((adapter, v, position, id) -> {
            String item = adapter.getItemAtPosition(position).toString();
            Map<String, Object> data1 = new HashMap<>();
            databaseLoginInfoConnection.collection("BoardGames")
                    .document(item)
                    .set(data1);
        });



        binding.buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* NavHostFragment.findNavController(GamesList.this)
                        .navigate(R.id.action_GamesList_to_GamesSaved);
                        */

                ArrayList<String> savedArrayList = new ArrayList<>();
                ArrayAdapter<String> tasks = new ArrayAdapter<>(getActivity(),
                        R.layout.gameslist_item_view, R.id.itemTextView,
                        savedArrayList);

                ListView listTasks = getActivity().findViewById(R.id.listView);
                listTasks.setAdapter(tasks);


                colRef.get()
                        .addOnCompleteListener(task -> {;
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());

                                    String value = document.getId();

                                    savedArrayList.add(value);
                                    //adds all doc IDs in SavedIdeas collection
                                    tasks.notifyDataSetChanged();
                                    //notifies adapter of change in ListView

                                    if (document.exists()) {

                                    } else {
                                        Log.d(TAG, "No such document");
                                    }
                                }
                            } else {
                                Log.d(TAG, "Error getting documents: ", task.getException());
                            }
                        });

            }
        });



        binding.gameListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* NavHostFragment.findNavController(GamesList.this)
                        .navigate(R.id.action_GamesList_to_GamesSaved);
                        */
                ArrayList<String> rankedArrayList = new ArrayList<>();
                ArrayAdapter<String> tasks = new ArrayAdapter<>(getActivity(),
                        R.layout.gameslist_item_view, R.id.itemTextView,
                        rankedArrayList);

                ListView listTasks = getActivity().findViewById(R.id.listView);
                listTasks.setAdapter(tasks);

                rankedRef.orderBy("rank", Query.Direction.ASCENDING).limit(10).get()
                        .addOnCompleteListener(task -> {;
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());

                                    rankedArrayList.add(document.getString("names"));
                                    //adds all doc IDs in SavedIdeas collection
                                    tasks.notifyDataSetChanged();
                                    //notifies adapter of change in ListView

                                    if (document.exists()) {

                                    } else {
                                        Log.d(TAG, "No such document");
                                    }
                                }
                            } else {
                                Log.d(TAG, "Error getting documents: ", task.getException());
                            }
                        });

            }
        });




    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}