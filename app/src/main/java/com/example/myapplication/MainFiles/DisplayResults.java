package com.example.myapplication.MainFiles;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.databinding.FragmentDisplayResultsBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Map;

/**Spinner (dropdown) Sourced from https://code.tutsplus.com/tutorials/how-to-add-a-dropdown-menu-in-android-studio--cms-37860
 * And https://developer.android.com/develop/ui/views/components/spinner
 *
 * https://stackoverflow.com/questions/20171148/how-can-i-use-string-variable-instead-of-using-r-id-webview1-in-android
 * Uses this to dynamically grab button ids and enable or disable them.
 *
 * https://www.youtube.com/watch?v=i-TqNzUryn8
 * Used to figure out how to grab from CSV files.
 *
 * https://firebase.google.com/docs/firestore/query-data/order-limit-data
 * Used for ordering queries
 */

public class DisplayResults extends Fragment {

    private FragmentDisplayResultsBinding binding;

    //false means search page state
    //true means displaying results
    private boolean isSearchState = false;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentDisplayResultsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    //This makes is such that our program can only store up to 25 results. If we are to have more, we must increase the size of these.
    //Will crash if we try and have more than 25.
    //Or switch over to data structures like linked lists.
    //Or dynamically create these arrays.
    private String rank[] = new String[26];
    private String bgg_url[] = new String[26];
    private long game_id[] = new long[26];
    private String names[] = new String[26];
    private String min_players[] = new String[26];
    private String max_players[] = new String[26];
    private String avg_time[] = new String[26];
    private String min_time[] = new String[26];
    private String max_time[] = new String[26];
    private String year[] = new String[26];
    private String avg_rating[] = new String[26];
    private String geek_rating[] = new String[26];
    private String num_votes[] = new String[26];
    private String image_url[] = new String[26];
    private String age[] = new String[26];
    private String mechanic[] = new String[26];
    private String owned[] = new String[26];
    private String category[] = new String[26];
    private String designer[] = new String[26];
    private String weight[] = new String[26];

    private int page = 1;
    private int fileCount = 0;
    private int max = fileCount;
    private FirebaseFirestore firebaseConnection;
    private boolean enableBoolean = true;
    private boolean isGameIdSearch = false;
    private boolean isMinSearch = false;
    private boolean isMaxSearch = false;
    private boolean isSortedLowToHigh = false;
    private boolean isSortedHighToLow = false;
    private boolean returnedSomething = false;

    /**
     * Sets whether or not buttons are enabled or not.
     * Takes count of how many files there are.
     * 5 files per page.
     * If there are less files than there are buttons, set the buttons we aren't using to be disabled.
     * If there are no more files on the next page, disable the nextButton.
     * If on the first page, disable previous button.
     *
     * https://stackoverflow.com/questions/20171148/how-can-i-use-string-variable-instead-of-using-r-id-webview1-in-android
     * Uses this to dynamically grab button ids and enable or disable them.
     *
     * https://www.youtube.com/watch?v=i-TqNzUryn8
     * Used to figure out how to grab from CSV files.
     *
     *
     */
    private void buttonLayout(){
        Button previousButton = getActivity().findViewById(R.id.previousPage);
        Button nextButton = getActivity().findViewById(R.id.nextPage);
        if(max> page){
            nextButton.setEnabled(true);
        }else{
            nextButton.setEnabled(false);
        }
        if(page == 1){
            previousButton.setEnabled(false);
        }else{
            previousButton.setEnabled(true);
        }
    }

    /**
     * clears results
     */
    private void clearResults(){
        String[] strings = new String[26];
        returnedSomething = false;
        enableBoolean = false;
        for(int i=0;i<fileCount;i++){
            rank[i] ="";
            bgg_url[i] = "";
            game_id[i] = 0;
            names[i] = "";
            min_players[i] = "";
            max_players[i] = "";
            avg_time[i] = "";
            min_time[i] = "";
            max_time[i] = "";
            year[i] = "";
            avg_rating[i] = "";
            geek_rating[i] = "";
            num_votes[i] = "";
            image_url[i] = "";
            age[i] = "";
            mechanic[i] = "";
            owned[i] = "";
            category[i] = "";
            designer[i] = "";
            weight[i] = "";
        }
        fileCount = 0;
        page = 1;
        max = 1;

    }

    /**
     * deals with what is shown on the page or not.
     * @param i alpha value for page items
     * @param pageValue enabled value for page items
     */
    public void hideOrShowSearchPage(int i,boolean pageValue){
        Switch gameIdSwitch = getActivity().findViewById(R.id.gameIdSwitch);
        Switch minSwitch = getActivity().findViewById(R.id.minSwitch);
        Switch maxSwitch = getActivity().findViewById(R.id.maxSwitch);
        Switch sortLowToHighSwitch = getActivity().findViewById(R.id.sortLowToHighSwitch);
        Switch sortHighToLowSwitch = getActivity().findViewById(R.id.highToLowSwitch);
        EditText gameIdEnter = getActivity().findViewById(R.id.gameIdEnter);
        EditText minEnter = getActivity().findViewById(R.id.minEnterText);
        EditText maxEnter = getActivity().findViewById(R.id.maxEnterText);
        TextView gameIdText = getActivity().findViewById(R.id.gameIdDisplayText2);
        TextView minMaxText = getActivity().findViewById(R.id.minMaxIdText);
        TextView sortLowToHighText = getActivity().findViewById(R.id.sortText);
        TextView sortHighToLowText = getActivity().findViewById(R.id.sortText2);
        TextView databaseText = getActivity().findViewById(R.id.Database);
        Spinner spinner = getActivity().findViewById(R.id.spinner);

        gameIdSwitch.setEnabled(pageValue);
        gameIdSwitch.setAlpha(i);
        gameIdSwitch.setChecked(!pageValue);

        minSwitch.setEnabled(pageValue);
        minSwitch.setAlpha(i);
        minSwitch.setChecked(!pageValue);

        maxSwitch.setEnabled(pageValue);
        maxSwitch.setAlpha(i);
        maxSwitch.setChecked(!pageValue);

        sortLowToHighSwitch.setEnabled(pageValue);
        sortLowToHighSwitch.setAlpha(i);
        sortLowToHighSwitch.setChecked(!pageValue);

        sortHighToLowSwitch.setEnabled(pageValue);
        sortHighToLowSwitch.setAlpha(i);
        sortHighToLowSwitch.setChecked(!pageValue);

        gameIdEnter.setAlpha(i);
        gameIdEnter.setEnabled(pageValue);

        minEnter.setAlpha(i);
        minEnter.setEnabled(pageValue);

        maxEnter.setAlpha(i);
        maxEnter.setEnabled(pageValue);

        gameIdText.setAlpha(i);
        gameIdText.setEnabled(pageValue);

        minMaxText.setAlpha(i);
        sortHighToLowText.setAlpha(i);
        sortLowToHighText.setAlpha(i);

        databaseText.setAlpha(i);
        spinner.setAlpha(i);
        spinner.setEnabled(pageValue);
    }

    /**
     * Setting up the page. Essentially if you're in search mode, set everything that pertaints to results to invisible and disabled. Set result to enabled and visible.
     * If you're in results mode, set everything that pertains to searching and set it invisible and disabled. Set search to enabled and visible.
     * @param i Alpha value. If 0, then set these buttons and text to be invisible. If 1, then set them to be visible.
     */
    public void searchPageLayout(int i){
        Button previousButton = getActivity().findViewById(R.id.previousPage);
        Button nextButton = getActivity().findViewById(R.id.nextPage);
        TextView pageShow = getActivity().findViewById(R.id.PageText);
        TextView gamesFound = getActivity().findViewById(R.id.gamesFound);
        previousButton.setAlpha(i);
        nextButton.setAlpha(i);
        gamesFound.setAlpha(i);
        pageShow.setAlpha(i);

        boolean pageValue = false;
        if(i==0){
            pageValue = true;
        }else if(i==1){
            pageValue = false;
        }

        //Flip values for these.
        //Due to how I had set up these pages, it is easier to flip the value and then set.
        if(i==0){
            i = 1;
        }else{
            i = 0;
        }
        hideOrShowSearchPage(i,pageValue);
    }

    /**
     * updates page statistics
     */
    private void updateResults(){
        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);

        TextView fileCountText = getActivity().findViewById(R.id.gamesFound);
        fileCountText.setText("Games found " + (fileCount - 1));
    }

    private void storeData(Map<String, Object> data){
        rank[fileCount - 1] = String.valueOf(data.get("rank")) ;
        bgg_url[fileCount - 1] = (String) data.get("bgg_url");
        game_id[fileCount - 1] = (long) data.get("game_id");
        names[fileCount - 1] = (String) data.get("names");
        min_players[fileCount - 1] = String.valueOf(data.get("min_players"));
        max_players[fileCount - 1] = String.valueOf(data.get("max_players"));
        avg_time[fileCount - 1] = String.valueOf(data.get("avg_time"));
        min_time[fileCount - 1] = (String) data.get("min_time");
        max_time[fileCount - 1] =(String) data.get("max_time");
        year[fileCount - 1] = (String) data.get("year");
        avg_rating[fileCount - 1] = (String) data.get("avg_rating");
        geek_rating[fileCount - 1] = (String) data.get("geek_rating");
        num_votes[fileCount - 1] = (String) data.get("num_votes");
        image_url[fileCount - 1] = (String) data.get("image_url");
        age[fileCount - 1] = (String) data.get("age");
        mechanic[fileCount - 1] = (String) data.get("mechanic");
        owned[fileCount - 1] = (String) data.get("owned");
        category[fileCount - 1] = (String) data.get("category");
        designer[fileCount - 1] = (String) data.get("designer");
        weight[fileCount - 1] = (String) data.get("weight");
    }

    /**
     * gets results from query. Stores in string arrays
     * Will do
     * Game id search
     * Minimum, maximum player search
     * Sorting avg time high to low and vice versa.
     *
     */
    private void getResults() {
        Spinner spinner = getActivity().findViewById(R.id.spinner);
        String dataSet = "data" + spinner.getSelectedItem().toString();
        //Pull entire dataset
        TextView displayText = getActivity().findViewById(R.id.resultDisplay);
        displayText.setText("One Moment");
        if (!(isGameIdSearch)) {
            //By default off.
            long minPlayers = 0;
            long maxPlayers = 9999999;
            if(isMinSearch){
                EditText minTextHolder = getActivity().findViewById(R.id.minEnterText);
                if(!minTextHolder.getText().toString().equals("")){
                minPlayers = Long.parseLong(minTextHolder.getText().toString());
                }
            }
            if(isMaxSearch){
                EditText maxTextHolder = getActivity().findViewById(R.id.maxEnterText);
                if(!maxTextHolder.getText().toString().equals("")){
                    maxPlayers = Long.parseLong(maxTextHolder.getText().toString());
                }
            }
            //Sourced from in app tutorial from android studio.
            //And https://firebase.google.com/docs/firestore/query-data/order-limit-data
            if(!(isSortedHighToLow || isSortedLowToHigh)){
                long finalMinPlayers = minPlayers;
                long finalMaxPlayers = maxPlayers;
                firebaseConnection.collection(dataSet)
                        .orderBy("rank", Query.Direction.ASCENDING)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    fileCount = page;
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Map<String, Object> data = document.getData();
                                        long maxData = (long) data.get("max_players");
                                        long minData = (long) data.get("min_players");
                                        if(isMaxSearch&&isMinSearch){
                                            if ((finalMinPlayers == minData) && (finalMaxPlayers == maxData)) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }
                                        else if(isMinSearch){
                                            if (finalMinPlayers == minData) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }
                                        else if(isMaxSearch){
                                            if (finalMaxPlayers == maxData) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }else{
                                            storeData(data);
                                            fileCount++;
                                            returnedSomething = true;
                                        }
                                    }
                                    displayResults();
                                    max = fileCount - 1;
                                    buttonLayout();
                                    updateResults();
                                } else {
                                }
                            }
                        });
            }else if(isSortedHighToLow){
                long finalMinPlayers = minPlayers;
                long finalMaxPlayers = maxPlayers;
                firebaseConnection.collection(dataSet).orderBy("avg_time", Query.Direction.DESCENDING)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    fileCount = page;
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Map<String, Object> data = document.getData();
                                        long maxData = (long) data.get("max_players");
                                        long minData = (long) data.get("min_players");
                                        if(isMaxSearch&&isMinSearch){
                                            if ((finalMinPlayers == minData) && (finalMaxPlayers == maxData)) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }
                                        else if(isMinSearch){
                                            if (finalMinPlayers == minData) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }
                                        else if(isMaxSearch){
                                            if (finalMaxPlayers == maxData) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }else{
                                            storeData(data);
                                            fileCount++;
                                            returnedSomething = true;
                                        }
                                    }
                                    displayResults();
                                    max = fileCount - 1;
                                    buttonLayout();
                                    updateResults();
                                } else {
                                }
                            }
                        });
            }
            else if(isSortedLowToHigh){
                long finalMinPlayers = minPlayers;
                long finalMaxPlayers = maxPlayers;
                firebaseConnection.collection(dataSet).orderBy("avg_time", Query.Direction.ASCENDING)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    fileCount = page;
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Map<String, Object> data = document.getData();
                                        long maxData = (long) data.get("max_players");
                                        long minData = (long) data.get("min_players");
                                        if(isMaxSearch&&isMinSearch){
                                            if ((finalMinPlayers == minData) && (finalMaxPlayers == maxData)) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }
                                        else if(isMinSearch){
                                            if (finalMinPlayers == minData) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }
                                        else if(isMaxSearch){
                                            if (finalMaxPlayers == maxData) {
                                                storeData(data);
                                                fileCount++;
                                                returnedSomething = true;
                                            }
                                        }else{
                                            storeData(data);
                                            fileCount++;
                                            returnedSomething = true;
                                        }
                                    }
                                    displayResults();
                                    max = fileCount - 1;
                                    buttonLayout();
                                    updateResults();
                                } else {
                                }
                            }
                        });
            }
        }else if(isGameIdSearch){
            EditText userInput = getActivity().findViewById(R.id.gameIdEnter);
            long game_idSearch;
            if(!userInput.getText().toString().equals("")){
                game_idSearch = Long.parseLong(userInput.getText().toString());
                firebaseConnection.collection(dataSet)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Map<String, Object> data = document.getData();
                                        long checkData = Integer.parseInt(String.valueOf(data.get("game_id")));
                                        if(checkData == game_idSearch){
                                            fileCount = page;
                                            storeData(data);
                                            returnedSomething = true;
                                        }
                                    }
                                    if(returnedSomething){
                                        displayResults();
                                        max = fileCount;
                                        buttonLayout();
                                        updateResults();
                                    }else{
                                        displayText.setText("No Results");
                                    }

                                }
                            }
                        });
            }else{
                Toast.makeText(getActivity(),"Error, invalid input", Toast.LENGTH_SHORT);
            }

        }
    }

    /**
     * Result button action.
     */
    public void resultButtonAction(){
        //If it is result state
        //Enter and set it to search state
        if(isSearchState){
            //Reset button
            Button resultButton = getActivity().findViewById(R.id.resultButton);
            resultButton.setText("get Results");
            //Undo display results
            TextView resultText = getActivity().findViewById(R.id.resultDisplay);
            resultText.setText("");
            //Set back to one
            page = 1;
            fileCount = 0;
            //Set to result state
            isSearchState = false;
            //Clear page for search mode
            clearResults();
            //Set page for search mode
            searchPageLayout(0);
            isGameIdSearch = false;
            isMinSearch = false;
            isMaxSearch = false;
            isSortedLowToHigh = false;
            isSortedHighToLow = false;
            enableBoolean = true;
        }else {
            //Set page to search display layout
            searchPageLayout(1);
            //Get results
            getResults();
            //Redo button
            Button resultButton = getActivity().findViewById(R.id.resultButton);
            resultButton.setText("Back to Search");
            buttonLayout();
            displayResults();
            updateResults();
            //Reset boolean values
            isSearchState = true;
            enableBoolean = true;
        }
    }

    /**
     *
     * sets up stage for the app page.
     * @param view
     * @param savedInstanceState
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        page = 1;

        buttonLayout();
        searchPageLayout(0);

        //sourced from https://developer.android.com/develop/ui/views/components/spinner
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.database_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        firebaseConnection = FirebaseFirestore.getInstance();

        Button nextButton = getActivity().findViewById(R.id.nextPage);
        nextButton.setOnClickListener(buttonAction -> nextButtonAction());

        Button previousButton = getActivity().findViewById(R.id.previousPage);
        previousButton.setOnClickListener(buttonAction -> previousButtonAction());

        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);

        TextView fileCountText = getActivity().findViewById(R.id.FileCount);
        fileCountText.setText("Games found " + fileCount);

        Button resultButton = getActivity().findViewById(R.id.resultButton);
        resultButton.setOnClickListener(buttonAction -> resultButtonAction());

        Switch gameIdSwitch = getActivity().findViewById(R.id.gameIdSwitch);
        gameIdSwitch.setOnClickListener(buttonAction-> {
            //Flip enable boolean.
            //Originally true
            //If set to false
            //Then disable other buttons.
            enableBoolean = !enableBoolean;
            //Flip gamesearch boolean
            isGameIdSearch = !isGameIdSearch;
            //Disable other buttons.
            Switch minSwitch = getActivity().findViewById(R.id.minSwitch);
            Switch maxSwitch = getActivity().findViewById(R.id.maxSwitch);
            Switch sortLowToHighSwitch = getActivity().findViewById(R.id.sortLowToHighSwitch);
            Switch sortHighToLowSwitch = getActivity().findViewById(R.id.highToLowSwitch);
            minSwitch.setEnabled(enableBoolean);
            maxSwitch.setEnabled(enableBoolean);
            sortHighToLowSwitch.setEnabled(enableBoolean);
            sortLowToHighSwitch.setEnabled(enableBoolean);
        });

        Switch minSwitch = getActivity().findViewById(R.id.minSwitch);
        minSwitch.setOnClickListener(buttonAction -> {
            //Flip value of whether or not it is minimum search
            isMinSearch = !isMinSearch;
        });

        Switch maxSwitch = getActivity().findViewById(R.id.maxSwitch);
        maxSwitch.setOnClickListener(buttonAction -> {
            //Flip value of whether or not it is max search
            isMaxSearch = !isMaxSearch;
        });

        Switch sortLowToHigh = getActivity().findViewById(R.id.sortLowToHighSwitch);
        sortLowToHigh.setOnClickListener(buttonAction->{
            //Flip value of whether or not it is sorted low to high
            isSortedLowToHigh = !isSortedLowToHigh;
            //Disable high to low search
            Switch sortedHighToLow = getActivity().findViewById(R.id.highToLowSwitch);
            sortedHighToLow.setEnabled(!isSortedLowToHigh);
        } );

        Switch sortHighToLow = getActivity().findViewById(R.id.highToLowSwitch);
        sortHighToLow.setOnClickListener(buttonAction->{
            //Flip value of whether or not it is sorted high to low
            isSortedHighToLow = !isSortedHighToLow;
            //Disable low to high search capabilities.
            Switch sortedLowToHigh = getActivity().findViewById(R.id.sortLowToHighSwitch);
            sortedLowToHigh.setEnabled(!isSortedHighToLow);
        } );

    }

    /**
     * displays the results on the page depending on what page the user has selected.
     */
    private void displayResults(){
        TextView resultText = getActivity().findViewById(R.id.resultDisplay);
        int iteratePage = page;
        String result = "";
        if(returnedSomething) {
            result =
                    "\n Rank: " + rank[iteratePage - 1]
                            + "\n bgg_url: " + bgg_url[iteratePage - 1]
                            + "\n game_id: " + game_id[iteratePage - 1]
                            + "\n names: " + names[iteratePage - 1]
                            + "\n min_players: " + min_players[iteratePage - 1]
                            + "\n max_players: " + max_players[iteratePage - 1]
                            + "\n avg_time: " + avg_time[iteratePage - 1]
                            + "\n min_time: " + min_time[iteratePage - 1]
                            + "\n max_time: " + max_time[iteratePage - 1]
                            + "\n year: " + year[iteratePage - 1]
                            + "\n avg_rating: " + avg_rating[iteratePage - 1]
                            + "\n geek_rating: " + geek_rating[iteratePage - 1]
                            + "\n num_votes: " + num_votes[iteratePage - 1]
                            + "\n image_url: " + image_url[iteratePage - 1]
                            + "\n age: " + age[iteratePage - 1]
                            + "\n mechanic: " + mechanic[iteratePage - 1]
                            + "\n owned: " + owned[iteratePage - 1]
                            + "\n category: " + category[iteratePage - 1]
                            + "\n designer: " + designer[iteratePage - 1]
                            + "\n weight: " + weight[iteratePage - 1];
        }
        resultText.setText(result);

    }

    /**
     * Increment page, redo button layout, set text to correct page.
     */
    public void nextButtonAction(){
        page++;
        buttonLayout();
        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);
        displayResults();
    }
    /**
     * Decrement page, redo button layout, set text to correct page.
     */
    public void previousButtonAction(){
        page--;
        buttonLayout();
        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);
        displayResults();
    }
}