package com.example.myapplication.MainFiles;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class YouTubeActivity extends YouTubeBaseActivity {

    Button btnVideo1, btnVideo2, btnVideo3, btnVideo4, btnVideo5, btnVideo6, btnVideo7;

    YouTubePlayerView youTubePlayerView;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    YouTubePlayer mYouTubePlayer;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube);

        final String videoId = "fKxG8KjH1Qg";

        btnVideo1 = findViewById(R.id.video1);
        btnVideo2 = findViewById(R.id.video2);
        btnVideo3 = findViewById(R.id.video3);
        btnVideo4 = findViewById(R.id.video4);
        btnVideo5 = findViewById(R.id.video5);
        btnVideo6 = findViewById(R.id.video6);
        btnVideo7 = findViewById(R.id.video7);

        youTubePlayerView = findViewById(R.id.YoutubePlayerView);

        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                mYouTubePlayer = youTubePlayer;
                mYouTubePlayer.cueVideo(videoId);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

        btnVideo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYouTubePlayer.cueVideo("fKxG8KjH1Qg");
            }

        });
        //8TuRYV71Rgo
        btnVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYouTubePlayer.cueVideo("K2t5wYJ8iXM");
            }

        });
        btnVideo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYouTubePlayer.cueVideo("cFVM_hwh56s");
            }

        });
        btnVideo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYouTubePlayer.cueVideo("bSVdYQFzFSs");
            }

        });
        btnVideo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYouTubePlayer.cueVideo("JBZsLbAF8bc");
            }

        });
        btnVideo6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {mYouTubePlayer.cueVideo("q-CMWujMIIE");}

        });
        btnVideo7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {mYouTubePlayer.cueVideo("S1yd4VZ9e88");}

        });

        youTubePlayerView.initialize("AIzaSyDoAULyX7xAmS4H6h7zJnJgFnp6md9kMg8", onInitializedListener);

    }
    private void youTubePlayerSetup(){


    }
}