package com.example.myapplication.MainFiles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.databinding.FragmentRequiredPageBinding;
import com.google.firebase.firestore.AggregateQuery;
import com.google.firebase.firestore.AggregateQuerySnapshot;
import com.google.firebase.firestore.AggregateSource;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * This class deals with pulling from the raw folder and pushing to the database
 */
public class RequiredPage extends Fragment {

    private FragmentRequiredPageBinding binding;
    private int page = 1;
    private int fileCount = 3;
    private int max = fileCount/5 + 1;
    private FirebaseFirestore firebaseConnection;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentRequiredPageBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Sets whether or not buttons are enabled or not.
     * Takes count of how many files there are.
     * 5 files per page.
     * If there are less files than there are buttons, set the buttons we aren't using to be disabled.
     * If there are no more files on the next page, disable the nextButton.
     * If on the first page, disable previous button.
     *
     * https://stackoverflow.com/questions/20171148/how-can-i-use-string-variable-instead-of-using-r-id-webview1-in-android
     * Uses this to dynamically grab button ids and enable or disable them.
     *
     * https://www.youtube.com/watch?v=i-TqNzUryn8
     * Used to figure out how to grab from CSV files.
     *
     *
     */
    private void buttonLayout(){
        Button previousButton = getActivity().findViewById(R.id.previousPage);
        Button nextButton = getActivity().findViewById(R.id.nextPage);
        if(max> page){
            nextButton.setEnabled(true);
        }else{
            nextButton.setEnabled(false);
        }
        if(page == 1){
            previousButton.setEnabled(false);
        }else{
            previousButton.setEnabled(true);
        }
        String buttonConcatenator = "button";
        int buttonCounter = 5;
        int count = 0;
        if(((page * 5) > fileCount)||(fileCount==0)) {
            count = ((page * 5) - fileCount);
            if(fileCount==0){
                count=5;
            }
            //Enough files to fill this page
            //PAGE IS EMPTY OR NOT FULLY FILLED
            buttonConcatenator = "button";
            while (count > 0) {
                buttonConcatenator += buttonCounter;
                //Sourced https://stackoverflow.com/questions/20171148/how-can-i-use-string-variable-instead-of-using-r-id-webview1-in-android
                int id = getResources().getIdentifier(buttonConcatenator, "id", getActivity().getPackageName());
                Button buttonEnabler = (Button) getActivity().findViewById(id);
                //End of source
                buttonEnabler.setEnabled(false);
                buttonConcatenator = "button";
                buttonCounter--;
                count--;
            }
        }else{
            count = 5;
            while (count > 0) {
                buttonConcatenator += buttonCounter;
                //Sourced https://stackoverflow.com/questions/20171148/how-can-i-use-string-variable-instead-of-using-r-id-webview1-in-android
                int id = getResources().getIdentifier(buttonConcatenator, "id", getActivity().getPackageName());
                Button buttonEnabler = (Button) getActivity().findViewById(id);
                //End of source
                buttonEnabler.setEnabled(true);
                buttonConcatenator = "button";
                buttonCounter--;
                count--;
            }
        }
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        page = 1;
        buttonLayout();

        firebaseConnection = FirebaseFirestore.getInstance();

        Button nextButton = getActivity().findViewById(R.id.nextPage);
        nextButton.setOnClickListener(buttonAction -> nextButtonAction());

        Button previousButton = getActivity().findViewById(R.id.previousPage);
        previousButton.setOnClickListener(buttonAction -> previousButtonAction());

        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);

        TextView fileCountText = getActivity().findViewById(R.id.FileCount);
        fileCountText.setText("Files found " + fileCount);

        Button firstButton = getActivity().findViewById(R.id.button1);
        firstButton.setOnClickListener(buttonAction-> {
            try {
                pullFromRawToDatabase("bgg_db_1806");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Button secondButton = getActivity().findViewById(R.id.button2);
        secondButton.setOnClickListener(buttonAction-> {
            try {
                pullFromRawToDatabase("bgg_db_2017_04");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Button thirdButton = getActivity().findViewById(R.id.button3);
        thirdButton.setOnClickListener(buttonAction-> {
            try {
                pullFromRawToDatabase("bgg_db_2018_01");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Increment page, redo button layout, set text to correct page.
     */
    public void nextButtonAction(){
        page++;
        buttonLayout();
        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);
    }
    /**
     * Decrement page, redo button layout, set text to correct page.
     */
    public void previousButtonAction(){
        page--;
        buttonLayout();
        TextView pageTracker = getActivity().findViewById(R.id.PageText);
        pageTracker.setText("Page "+page+"/"+max);
    }

    private static int countVariable = 1;

    /**
     * sourced https://www.youtube.com/watch?v=i-TqNzUryn8
     * Needed this video in order to figure out how to read from csv files.
     *
     * https://firebase.google.com/docs/firestore/query-data/aggregation-queries
     * For queries
     **/
    public void pullFromRawToDatabase(String file) throws IOException {
        InputStream inputTime = null;
        String collectionPath = "";
        if (file.equals("bgg_db_1806")) {
            inputTime = getResources().openRawResource(R.raw.bgg_db_1806);
            collectionPath = "data1806";
        } else if (file.equals("bgg_db_2017_04")) {
            inputTime = getResources().openRawResource(R.raw.bgg_db_2017_04);
            collectionPath = "data2017";
        } else if (file.equals("bgg_db_2018_01")) {
            inputTime = getResources().openRawResource(R.raw.bgg_db_2018_01);
            collectionPath = "data2018";
        }

        CollectionReference collection = firebaseConnection.collection(collectionPath);
        Query query = collection;
        AggregateQuery aggregateQuery = query.count();
        String finalCollectionPath = collectionPath;
        InputStream finalInputTime = inputTime;
        aggregateQuery.get(AggregateSource.SERVER).addOnCompleteListener(task ->{
            if(task.isSuccessful()){
                AggregateQuerySnapshot snapshot = task.getResult();
                countVariable = (int) snapshot.getCount();

                Toast.makeText(getActivity(), "Files on database:" + countVariable, Toast.LENGTH_SHORT).show();
                if(countVariable == 0) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(finalInputTime, Charset.forName("UTF-8"))
                    );
                    String line = "";
                    while (true) {
                        try {
                            if (!((line = reader.readLine()) != null)) break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String[] token = line.split(",");

                        Map<String, Object> data = new HashMap<>();
                        data.put("rank", Integer.valueOf(token[0]));
                        data.put("bgg_url", token[1]);
                        data.put("game_id", Integer.valueOf(token[2]));
                        data.put("names", token[3]);
                        data.put("min_players", Integer.valueOf(token[4]));
                        data.put("max_players", Integer.valueOf(token[5]));
                        data.put("avg_time", Integer.valueOf(token[6]));
                        data.put("min_time", token[7]);
                        data.put("max_time", token[8]);
                        data.put("year", token[9]);
                        data.put("avg_rating", token[10]);
                        data.put("geek_rating", token[11]);
                        data.put("num_votes", token[12]);
                        data.put("image_url", token[13]);
                        data.put("age", token[14]);
                        data.put("mechanic", token[15]);
                        data.put("owned", token[16]);
                        data.put("category", token[17]);
                        data.put("designer", token[18]);
                        data.put("weight", token[19]);

                        firebaseConnection.collection(finalCollectionPath).add(data);
                    }
                    Toast.makeText(getActivity(), "Successfully added files!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), "Some files already exist on the database with that name.", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}